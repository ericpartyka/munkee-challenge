//
//  MHelper.m
//  Munkee
//
//  Created by Eric Partyka on 10/27/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "MHelper.h"

@implementation MHelper

+ (UIImage *)theLayer
{
    NSString *imgBundlePath=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"beach"];
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:imgBundlePath];
    CALayer *mask = [CALayer layer];
    mask.contents = (id)[image  CGImage];
    mask.frame = CGRectMake(0, 0, 250, 250);
    
    return (id)[image  CGImage];
}

@end
