//
//  main.m
//  Munkee
//
//  Created by Eric Partyka on 10/28/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MAppDelegate class]));
    }
}
