//
//  MViewController.m
//  Munkee
//
//  Created by Eric Partyka on 10/28/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "MViewController.h"
#import "MHelper.h"

@interface MViewController ()

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UIImageView *theImageView;
@property (strong, nonatomic) IBOutlet UISlider *theSlider;


@end

@implementation MViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self configureView];
    
}

#pragma mark - Private Methods

- (void)configureView
{
    self.theImageView.layer.contents = [MHelper theLayer];
    self.theSlider.minimumValue = 0.5;
    self.theSlider.maximumValue = 1.5;
    self.theSlider.value = 1.0;
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)scaleImage:(UISlider *)sender
{
   self.theImageView.transform = CGAffineTransformMakeScale(sender.value, sender.value);
}
@end
