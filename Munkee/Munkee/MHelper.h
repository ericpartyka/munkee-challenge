//
//  MHelper.h
//  Munkee
//
//  Created by Eric Partyka on 10/27/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface MHelper : NSObject

+ (UIImage *)theLayer;

@end
